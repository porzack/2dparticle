package particle2d.jy89hui.main;

import java.util.Random;

import jutils.jy89hui.general.logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import particle2d.jy89hui.particle.ParticleForce;
import particle2d.jy89hui.particle.ParticleManager;
import particle2d.jy89hui.particle.Path;


/**
 * Run time = ((tickspervsecond*#SimTime)*(#numforces*timefor1forceand1particle)) * 1/(inc^2)
 * VAREA = {500,500}
 * TICKSPERVSECOND=50
 * SEED 1 SimulationTime= 500s, Forces = 100, 
 * 
 * 
 * 
 * 
 * @author Zack
 *
 */
public class TMain {

	public static void main(String[] args){
		test1();
	}
	
	public static void comp1(){
		Particle2D.init();
		Random r = new Random();
		r.setSeed(1);
		Particle2D p2d = Particle2D.instance;
		ParticleManager pm = p2d.getParticleManager();
		for(int x=0; x<100; x++){
			p2d.genForce(r.nextDouble()*500, r.nextDouble()*500, 5f,0,0,ParticleForce.TYPE_GRAVITATIONAL);
		}
		p2d.createNewParticleDisplay();
		p2d.PATH_SIMULATION_TIME = 500f;
		double inc = 1f;
		for (double x = 5; x<500; x+=inc){
			for (double y= 5; y<500; y+=inc){
				p2d.getParticleDisplay().clearPaths();
				Path p = new Path();
				p2d.getParticleDisplay().pathsToDraw.add(p);
				if(!sim1(p,x,y)){
					logger.log("Found at "+x+", "+y);
				}
				
			}
		}
		
		p2d.run();
		
	}
	public static boolean sim1(Path p,double x, double y){
		p.performAnalysis(x, y, 0, 0, Particle2D.instance.getForces(), 1000f);
		//double[] loc = p.data.get(p.data.size()-1);
		for(double[] loc : p.data){
			if (loc[0]<0 || loc[0]>500){
				//logger.log("1  "+loc[0]);
				return true;
			}
			if (loc[1]<0 || loc[1]>500){
				//logger.log("2  "+loc[1]);
				return true;
			}
		}
		
		return false;
	}
	public static void test1(){
		Particle2D.init();
		Random r = new Random();
		r.setSeed(1);
		Particle2D p2d = Particle2D.instance;
		ParticleManager pm = p2d.getParticleManager();
		for(int x=0; x<0; x++){
			p2d.genForce(r.nextDouble()*500, r.nextDouble()*500, 5f,0,0,ParticleForce.TYPE_GRAVITATIONAL);
		}
		p2d.createNewParticleDisplay();
		int sqrtlines=80;
		int simtime = 25;
		for(int x=0; x<sqrtlines; x++){
			for (int y=0; y<sqrtlines; y++) {
				pm.createParticle((double)Math.random()*500, (double)Math.random()*500);
			}
			
		}
		
		Particle2D.instance.genForce(Particle2D.VIRTUALAREA_X/2, Particle2D.VIRTUALAREA_Y/2, 10, 0, 0, ParticleForce.TYPE_GRAVITATIONAL);
		
		Path p1 = new Path();
		Path p2 = new Path();
		Path p3 = new Path();
		//p1.performAnalysis(123, 295, 0, 0, p2d.getForces(), 500);
		//p2d.getParticleDisplay().pathsToDraw.add(p1);
		p2d.run();
	}
	
}
