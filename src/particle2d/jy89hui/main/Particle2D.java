package particle2d.jy89hui.main;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

import particle2d.jy89hui.particle.ParticleForce;
import particle2d.jy89hui.particle.ParticleManager;
import particle2d.jy89hui.visuals.ParticleDisplay;

public class Particle2D {

	public static int VIRTUALAREA_X=500;
	public static int VIRTUALAREA_Y=500;
	public static int DISPLAY_WIDTH=1000;
	public static int DISPLAY_HEIGHT=1000;
	public static double PATH_SIMULATION_TIME = 100;
	public static final int PARTICLE_SIZE = 1;
	public static final int FORCE_SIZE = 10;
	public static Particle2D instance;
	private ParticleManager particleManager;
	private ParticleDisplay particleDisplay;
	private CommandManager commandManager;
	public static final String TITLE = "2DParticleLib";
	public double timeinSeconds = 0;
	private ArrayList<ParticleForce> forces = new ArrayList<ParticleForce>();
	
	public Particle2D(){
		this.createNewParticleManager();
	}
	
	
	public static void init(){
		instance=new Particle2D();
		
	}

	public String getTime(){
		int minutes =0;
		double seconds = timeinSeconds;
		if(seconds>60){
		minutes = (int) Math.floor((seconds/60.0));
		seconds = seconds%60;
		}
		return minutes+":"+(int)seconds;
	}
	public void run(){
		for (;;){
			double inc = 1.0/particleManager.tickratePerVSecond;
			timeinSeconds+=inc;
			particleManager.updateParticles(forces);
			particleDisplay.fullydeleteselectedforces();
			
		}
	}
	
	public void defineVirtualBounds(int areax, int areay){
		VIRTUALAREA_X=areax;
		VIRTUALAREA_Y=areay;
	}
	public ParticleDisplay createNewParticleDisplay(){
		this.particleDisplay = new ParticleDisplay(TITLE);
		Thread thread = new Thread(){
			@Override
			public void run(){
				AppGameContainer app;
				try {
					app = new AppGameContainer(particleDisplay);
					app.setDisplayMode(DISPLAY_WIDTH, DISPLAY_HEIGHT, false);
					app.setShowFPS(true);
					app.start();
				} catch (SlickException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		thread.start();
		return this.particleDisplay;
	}
	public ParticleDisplay getParticleDisplay(){
		return this.particleDisplay;
	}
	public static String particleToString(double[] particle){
		String s = "";
		s+="{";
		s+= ("x: "+particle[ParticleManager.PARTICLEDATA_XPOS]);
		s+= (" y: "+particle[ParticleManager.PARTICLEDATA_YPOS]);
		s+= (" xvel: "+particle[ParticleManager.PARTICLEDATA_XVEL]);
		s+= (" yvel: "+particle[ParticleManager.PARTICLEDATA_YVEL]);
		s+= "}";
		return s;
	}
	public static String forceToString(double[] force){
		String s = "";
		s+= "{";
		s+= ("x: "+force[ParticleForce.FORCEDATA_XPOS]);
		s+= (" y: "+force[ParticleForce.FORCEDATA_YPOS]);
		s+= (" Amplitude: "+force[ParticleForce.FORCEDATA_POWER]);
		s+= (" DataSlot1: "+force[ParticleForce.FORCEDATA_DATASLOT1]);
		s+= (" DataSlot2: "+force[ParticleForce.FORCEDATA_DATASLOT2]);
		s+= (" Type: "+force[ParticleForce.FORCEDATA_TYPE]);
		s+= "}";
		return s;
	}
	public void setParticleDisplay(ParticleDisplay pd){
		this.particleDisplay=pd;
	}
	public CommandManager createNewCommandManager(){
		this.commandManager = new CommandManager(this);
		return this.commandManager;
	}
	public CommandManager getCommandManager(){
		return this.commandManager;
	}
	public ParticleManager createNewParticleManager(){
		this.particleManager=new ParticleManager();
		return this.particleManager;
	}
	public ParticleManager getParticleManager(){
		return this.particleManager;
	}
	public ArrayList<ParticleForce> getForces(){
		return this.forces;
	}
	public void setParticleManager(ParticleManager pm){
		this.particleManager=pm;
	}
	public ParticleForce genForce(double x, double y, double power, double dslot1, double dslot2, double type){
		ParticleForce p = ParticleForce.createNewParticle(new double[]{x,y,power,dslot1,dslot2,type});
		forces.add(p);
		return p;
	}
	public void clearForces(){
		forces = new ArrayList<ParticleForce>();
	}
	
}
