package particle2d.jy89hui.main;

import particle2d.jy89hui.particle.ParticleForce;
import particle2d.jy89hui.particle.ParticleManager;
import particle2d.jy89hui.particle.Path;
import particle2d.jy89hui.visuals.ParticleDisplay;
import jutils.jy89hui.ConsoleManager.Command;
import jutils.jy89hui.ConsoleManager.Console;
import jutils.jy89hui.general.logger;

public class CommandManager {
	private Console console;
	private Particle2D particle2d;
	private ParticleManager particleManager;
	private ParticleDisplay particleDisplay;
	public CommandManager(Particle2D particle2d){
		Console.createInstance("Console", 500, 700);
		console = Console.instance;
		this.particle2d=particle2d;
		this.particleManager=particle2d.getParticleManager();
		this.particleDisplay=particle2d.getParticleDisplay();
	}
	public void registerCommand(Command cmd){
		console.registerCommand(cmd);
	}
	public void registerBasics(){
		this.registerCommand(new Command("spawnParticle"){
			@Override
			public void called(String[] args){
				if (args.length==2){
					particleManager.createParticle(Double.parseDouble(args[0]), Double.parseDouble(args[1]));
				} else if (args.length==4){
					particleManager.createParticle(Double.parseDouble(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]));
				} else{
					logger.log("You have passed invalid arguments to the spawnParticle Command");
				}
			}
		});
		this.registerCommand(new Command("spawnForce"){
			@Override
			public void called(String[] args){
				if (args.length == 3){
					double x = Double.parseDouble(args[0]);
					double y = Double.parseDouble(args[1]);
					double power = Double.parseDouble(args[2]);
					particle2d.genForce(x, y, power, 0, 0, ParticleForce.TYPE_GRAVITATIONAL);
				} else if (args.length == 4){
					double x = Double.parseDouble(args[0]);
					double y = Double.parseDouble(args[1]);
					double power = Double.parseDouble(args[2]);
					double type = Double.parseDouble(args[3]);
					particle2d.genForce(x, y, power, 0, 0, type);
				} else{
					logger.log("You have passed invalid arguments to the spawnForce Command");
				}
				
			}
		});
		this.registerCommand(new Command("setTicksPerVSecond"){
			@Override
			public void called(String[] args){
					int val = Integer.parseInt(args[0]);
					particleManager.tickratePerVSecond=val;
			}
		});
		this.registerCommand(new Command("setVirtualArea"){
			@Override
			public void called(String[] args){
				if (args.length==2){
						int x = Integer.parseInt(args[0]);
						int y = Integer.parseInt(args[0]);
						Particle2D.VIRTUALAREA_X=x;
						Particle2D.VIRTUALAREA_Y=y;
				}else{
					logger.log("You have passed invalid arguments to the setVirtualArea Command");
				}
			}
		});
		this.registerCommand(new Command("list"){
			@Override
			public void called(String[] args){
				if (args[0].equalsIgnoreCase("particles")){
					for (double[] p : particleManager.getParticles()){
						logger.log(""+Particle2D.particleToString(p));
					}
				}else if (args[0].equalsIgnoreCase("forces")){
					for (ParticleForce f : particle2d.getForces()){
						logger.log(""+Particle2D.forceToString(f.getData()));
					}
				} else if (args[0].equalsIgnoreCase("paths")){
					logger.log("UNFINISHED");
				} else{
					logger.log("Invalid arguments");
				}
			}
		});
		this.registerCommand(new Command("genPath"){
			@Override
			public void called(String[] args){
				if (args.length==2){
					int x = Integer.parseInt(args[0]);
					int y = Integer.parseInt(args[1]);
					Path p = new Path();
					p.performAnalysis(x, y, 0, 0, Particle2D.instance.getForces(), Particle2D.PATH_SIMULATION_TIME);
					particleDisplay.pathsToDraw.add(p);
				} else if (args.length==3){
					int x = Integer.parseInt(args[0]);
					int y = Integer.parseInt(args[1]);
					int time = Integer.parseInt(args[2]);
					Path p = new Path();
					p.performAnalysis(x, y, 0, 0, Particle2D.instance.getForces(), time);
					particleDisplay.pathsToDraw.add(p);
				}
			}
		});
		this.registerCommand(new Command("getMouse"){
			@Override
			public void called(String[] args){
				int x = particleDisplay.gameContainer.getInput().getMouseX();
				int y = particleDisplay.gameContainer.getInput().getMouseY();
				logger.log("{"+x+", "+y+"}");
			}
		});
		
		
		this.registerCommand(new Command("spawnhhForce"){
			@Override
			public void called(String[] args){
				
			}
		});
	}

	
}
