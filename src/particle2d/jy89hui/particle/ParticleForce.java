package particle2d.jy89hui.particle;

import particle2d.jy89hui.main.Particle2D;
import jutils.jy89hui.general.logger;

public class ParticleForce {

	public static final int FORCEDATA_XPOS=0;
	public static final int FORCEDATA_YPOS=1;
	public static final int FORCEDATA_POWER=2;
	public static final int FORCEDATA_DATASLOT1=3;
	public static final int FORCEDATA_DATASLOT2=4;
	public static final int FORCEDATA_TYPE=5;
	public static final int FORCEDATA_LENGTH=6;
	public static final boolean PERVENT_PARTICLESUPER_SPEED=true;
	public static final int TYPE_GRAVITATIONAL =0;
	public static final int TYPE_FASTGRAVITATIONAL =1;
	public static final int TYPE_TWIRL =2;
	public static final int TYPE_COMPLEX_RING =3;
	
	
	private double[] data;
	public ParticleForce(double[] data){
		this.data=data;
	}
	public static double[] DEFAULT_GRAVITATIONAL(double x, double y, double[] data){
		int dx = (int) (data[FORCEDATA_XPOS]-x);
		int dy = (int) (data[FORCEDATA_YPOS]-y);
		int dx2 = dx*dx;
		int dy2 = dy*dy;
		int unsquarerooted = (dx2+dy2);
		double appliedPower = data[FORCEDATA_POWER]/unsquarerooted;
		double hyp = (double) Math.sqrt(unsquarerooted);
		double fx = (dx/hyp)*appliedPower;
		double fy = (dy/hyp)*appliedPower;
		if(PERVENT_PARTICLESUPER_SPEED){
			if (Double.isNaN((double) fx) || fx>10000000 || Double.isNaN((double) fy) || fy>10000000){
				fx=0;
				fy=0;
			}
		}
		return new double[]{fx,fy};
	}
	/**
	 * Same Method, just with a possibly faster but less accurate version of calculating the hypotenuse.
	 * Honestly no idea if this works or if it makes any difference, pretty much untested. 
	 * Not really a good idea to use this.
	 * @param x
	 * @param y
	 * @param data
	 * @return
	 */
	public static double[] DEFAULT_FASTGRAVITATIONAL(double x, double y, double[] data){
		int dx = (int) (data[FORCEDATA_XPOS]-x);
		int dy = (int) (data[FORCEDATA_YPOS]-y);
		double hyp = fastGetHyp(dx,dy);
		double unsquarerooted = (hyp*hyp);
		double appliedPower = data[FORCEDATA_POWER]/unsquarerooted;
		double fx = (dx/hyp)*appliedPower;
		double fy = (dy/hyp)*appliedPower;
		if(PERVENT_PARTICLESUPER_SPEED){
			if (Double.isNaN((double) fx) || fx>10000000 || Double.isNaN((double) fy) || fy>10000000){
				fx=0;
				fy=0;
			}
		}
		return new double[]{fx,fy};
	}
	public static double fastGetHyp (double a, double b){
		if (a<=b){
			return (double) (((0.41421)*a)+b);
		}else{
			return (double) (((0.41421)*b)+a);
		}
	}
	public static double[] DEFAULT_TWIRL(double x, double y, double[] data){
		double dx =  (data[FORCEDATA_XPOS]-x);
		double dy =  (data[FORCEDATA_YPOS]-y);
		int dx2=(int)(dx*dx);
		int dy2=(int)(dy*dy);
		int unsquarerooted = dx2+dy2;
		double dividingBy  = unsquarerooted / data[FORCEDATA_POWER];
		dx/=dividingBy;
		dy/=dividingBy;
		return new double[]{-dy,dx};
	}
	/**
	 * Ability to layer multiple DEFAULT_GRAVITATIONAL forces inside one. dataslot 1 and 2 are the forces for both rings
	 * @param x 
	 * @param y
	 * @param data
	 * @return
	 */
	public static double[] DEFAULT_COMPLEX_RING(double x, double y, double[] data){
		data[FORCEDATA_POWER] = data[FORCEDATA_DATASLOT1];
		double[] r1 = DEFAULT_GRAVITATIONAL(x,y,data);
		data[FORCEDATA_POWER] = data[FORCEDATA_DATASLOT2];
		double[] r2 = DEFAULT_GRAVITATIONAL(x,y,data);
		// Would use declerations but knownlength=2, Would have to use XPOS and YPOS, would add confusion
		// Add the vectors
		r1[0] += r2[0];
		r1[1] += r2[1];
		return r1;
	}
	
	
	
	public double[] getAppliedForce(double locx, double locy){
		logger.log("Error, getAppliedForce() was called without overriding method! Data: "+Particle2D.forceToString(data)); 
		return null;
	}
	
	
	public double[] getData(){
		return data;
	}
	public void setData(double[] data){
		this.data=data;
	}
	
	
	public static ParticleForce createNewParticle(double[] data){
		if (data.length == FORCEDATA_LENGTH){
			
			switch ((int)data[FORCEDATA_TYPE]){
			case TYPE_GRAVITATIONAL:
				return new ParticleForce(data){
					@Override
					public double[] getAppliedForce(double x, double y){
						return DEFAULT_GRAVITATIONAL(x,y,data);
					}
				};
			case TYPE_FASTGRAVITATIONAL:
				return new ParticleForce(data){
					@Override
					public double[] getAppliedForce(double x, double y){
						return DEFAULT_FASTGRAVITATIONAL(x,y,data);
					}
				};
			case TYPE_TWIRL:
				return new ParticleForce(data){
					@Override
					public double[] getAppliedForce(double x, double y){
						return DEFAULT_TWIRL(x,y,data);
					}
				};
			case TYPE_COMPLEX_RING:
				return new ParticleForce(data){
					@Override
					public double[] getAppliedForce(double x, double y){
						return DEFAULT_COMPLEX_RING(x,y,data);
					}
				};
			default:
				logger.log("You have initialized the ParticleForce class without a proper TYPE! Failed to redefine getAppliedForce Method!");
				return new ParticleForce(data);
			}
		}else{
			logger.log("Data provided to the createNewParticle Method is invalid! Problem: Length Needed length: "+FORCEDATA_LENGTH+" Length: "+data.length);
			return null;
		}
	}
	
}
