package particle2d.jy89hui.particle;

import java.util.ArrayList;

import particle2d.jy89hui.main.Particle2D;
import jutils.jy89hui.general.logger;

public class Path {
	public ArrayList<double[]> data = new ArrayList<double[]>();
	private ParticleManager manager;
	
	public Path(){
		this.manager = new ParticleManager();
	}
	
	public void performAnalysis(double x, double y, double vx, double vy, ArrayList<ParticleForce> forces, double vsecondsToSim){
		data = new ArrayList<double[]>();
		double[] particle = manager.createParticle(x, y, vx, vy);
		//logger.log("Performing path analysis on "+Particle2D.particleToString(particle)+"");
		int times = (int) Math.ceil(vsecondsToSim * manager.tickratePerVSecond);
		for(int time=0; time<times; time++){
			if (manager.getParticles().size()!=1){
				logger.fatalError("Error! Cannot simulate! There are an invalid number of particles in the mananager. Class= Path, Method= Perform Analsis");
				return;
			}
			manager.updateParticles(forces);
			// Should only be one particle as proven by the first check. Get(0) is totally fine.
			data.add(manager.getParticles().get(0).clone());
		}
		
	}
	
}
