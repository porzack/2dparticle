package particle2d.jy89hui.particle;

import java.util.ArrayList;

import particle2d.jy89hui.main.Particle2D;

public class ParticleManager {
	public static final int PARTICLEDATA_XPOS=0;
	public static final int PARTICLEDATA_YPOS=1;
	public static final int PARTICLEDATA_XVEL=2;
	public static final int PARTICLEDATA_YVEL=3;
	public static final int PARTICLEDATA_LENGTH = 4;
	public static double tickratePerVSecond=50;
	private ArrayList<double[]> particles;
	
	public ParticleManager(){
		particles = new ArrayList<double[]>();
		
	}
	public void updateParticles(ArrayList<ParticleForce> forces){
		double[] vec2d = {0,0};
		double lx;
		double ly;
		for(int particle =0; particle<particles.size(); particle++){
			double[] particledata = particles.get(particle);
			vec2d[0]=0f;
			vec2d[1]=0f;
			lx = particledata[PARTICLEDATA_XPOS];
			ly = particledata[PARTICLEDATA_YPOS];
			for(ParticleForce force : forces){
				vec2d = combineVec(force.getAppliedForce(lx,ly) , vec2d);
			}
			particledata[PARTICLEDATA_XVEL] += vec2d[0];
			particledata[PARTICLEDATA_YVEL] += vec2d[1];
			particledata[PARTICLEDATA_XPOS] += (particledata[PARTICLEDATA_XVEL]/tickratePerVSecond);
			particledata[PARTICLEDATA_YPOS] += (particledata[PARTICLEDATA_YVEL]/tickratePerVSecond);
		}
	}
	
	public double[] combineVec(double[] vec1, double[] vec2){
		return new double[]{vec1[0]+vec2[0],vec1[1]+vec2[1]};
	}
	public double[] createParticle(){
		return createParticle((double)(Math.random()*Particle2D.VIRTUALAREA_X),
				(double)(Math.random()*Particle2D.VIRTUALAREA_Y));
	}
	public double[] createParticle(double x, double y){
		return createParticle(x,y,0,0);
	}
	public double[] createParticle(double x, double y, double xvel, double yvel){
		double[] particle = new double[PARTICLEDATA_LENGTH];
		particle[PARTICLEDATA_XPOS]=x;
		particle[PARTICLEDATA_YPOS]=y;
		particle[PARTICLEDATA_XVEL]=xvel;
		particle[PARTICLEDATA_YVEL]=yvel;
		particles.add(particle);
		return particle;
	}
	public ArrayList<double[]> getParticles(){
		return this.particles;
	}
	
}
