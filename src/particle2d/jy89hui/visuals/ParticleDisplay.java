package particle2d.jy89hui.visuals;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import particle2d.jy89hui.main.Particle2D;
import jutils.jy89hui.general.logger;
import particle2d.jy89hui.particle.ParticleForce;
import particle2d.jy89hui.particle.ParticleManager;
import particle2d.jy89hui.particle.Path;

public class ParticleDisplay extends BasicGame{

	private final String MOUSEMODE_PLACE_ION = "ION";
	private final String MOUSEMODE_PLACE_FORCE = "FORCE";
	private boolean helpToggled=false;
	private String mode=MOUSEMODE_PLACE_ION;
	public GameContainer gameContainer=null;
	public boolean renderingPaused = false;
	public ArrayList<Path> pathsToDraw = new ArrayList<Path>();
	ParticleManager particleManager;
	public ParticleDisplay(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public void clearPaths(){
		pathsToDraw = new ArrayList<Path>();
	}
	@Override
	public void render(GameContainer gc, Graphics g) {
		try{
			zrender(gc,g);
		} catch(SlickException e){
			e.printStackTrace();
		} catch(ConcurrentModificationException e){
			// Literally the biggest piece of shit in the entire fucking language
			// Not gonna handle this shit.
		}
	}
	private void zrender(GameContainer gc, Graphics g) throws SlickException, ConcurrentModificationException{
		if (renderingPaused){
			return;
		}
		double x;
		double y;
		double power;
		int type;
		double[] force;
		double ratioX = (double)Particle2D.DISPLAY_WIDTH/Particle2D.VIRTUALAREA_X;
		double ratioY = (double)Particle2D.DISPLAY_HEIGHT/Particle2D.VIRTUALAREA_Y;
		for(Path p : pathsToDraw){
			for (double[] data : p.data){
				g.setColor(this.getColorBasedOnVel(data, 20, 20, 20));
				x=data[ParticleManager.PARTICLEDATA_XPOS];
				y=data[ParticleManager.PARTICLEDATA_YPOS];
				x*=ratioX;
				y*=ratioY;
				g.fillRect((float)x, (float)y, Particle2D.PARTICLE_SIZE, Particle2D.PARTICLE_SIZE);
			}
		}
		for (ParticleForce pforce : Particle2D.instance.getForces()){
			force = pforce.getData();
			x=force[ParticleForce.FORCEDATA_XPOS];
			y=force[ParticleForce.FORCEDATA_YPOS];
			type=(int)force[ParticleForce.FORCEDATA_TYPE];
			this.redefineColorbasedonType(g, type);
			x*=ratioX;
			y*=ratioY;
			g.fillRect((float)x- Particle2D.FORCE_SIZE/2, (float)y- Particle2D.FORCE_SIZE/2, Particle2D.FORCE_SIZE, Particle2D.FORCE_SIZE);
		}
		g.setColor(Color.red);
		for (double[] particle : particleManager.getParticles()){;
			x=particle[ParticleManager.PARTICLEDATA_XPOS];
			y=particle[ParticleManager.PARTICLEDATA_YPOS];
			x*=ratioX;
			y*=ratioY;
			g.fillRect((float)x, (float)y, Particle2D.PARTICLE_SIZE, Particle2D.PARTICLE_SIZE);
		}
		

		g.setColor(Color.white);
		g.drawString("Press H for help ", 20, Particle2D.DISPLAY_HEIGHT-20);
		g.drawString("Time: "+Particle2D.instance.getTime(), 10, 30);
		g.drawString("Particles: "+Particle2D.instance.getParticleManager().getParticles().size(), 10, 50);
		g.drawString("Forces: "+Particle2D.instance.getForces().size(), 10, 70);
		g.drawString("Paths: "+pathsToDraw.size(), 10, 90);
		if (alteringVSeconds) {
			g.setColor(Color.red);
			g.drawString("TicksPerVSecond: "+val, 10, 110);
		}
		else {
			g.drawString("TicksPerVSecond: "+particleManager.tickratePerVSecond, 10, 110);
		}
		if (helpToggled) {
			g.setColor(Color.gray);
			int border = 100;
			g.fillRect(border, border, Particle2D.DISPLAY_WIDTH-border*2, Particle2D.DISPLAY_HEIGHT-border*2);
			g.setColor(Color.white);
			g.drawString("Welcome to the help screen!", border+100, border+100);
			g.drawString("Commands:", border+100, border+150);
			g.drawString("H : Toggle Help Screen", border+130, border+170);
			g.drawString("P : Set place_mode to place particles", border+130, border+190);
			g.drawString("F : Set place_mode to be pre-defined gravitational forces", border+130, border+210);
			g.drawString("[ENTER] : Place type place_mode at the cursor location", border+120, border+230);
			g.drawString("L : Generate vector field diagram while killing performace", border+130, border+250);
			g.drawString("(Press C to clear)", border+200, border+270);
			g.drawString("A : Create a mock-path of where a particle would go", border+130, border+290);
			g.drawString("C : clear all paths", border+130, border+310);
			g.drawString("V : Edit sim speed. (TicksPerVSecond), press V,\n		 type in a number, press V again to set it", border+130, border+330);
			g.drawString("Have Fun! - Zachary Porter (gitlab.com/porzack)", border+100, border+390);

		}
	}
	@Override
	public void init(GameContainer gc) throws SlickException {
		this.particleManager = Particle2D.instance.getParticleManager();
		if (this.particleManager==null){
			logger.log("Warning! You should initialize the ParticleManager inside the Particle2D class before initializing the ParticleDisplay Class! Initializing it for you.");
			this.particleManager = Particle2D.instance.createNewParticleManager();
		}
		this.gameContainer=gc;

	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
		
	}
	public double[] getMousePos(){
		double ratioX = (double)Particle2D.VIRTUALAREA_X/(double)Particle2D.DISPLAY_WIDTH;
		double ratioY = (double)Particle2D.VIRTUALAREA_Y/(double)Particle2D.DISPLAY_HEIGHT;
		double x = ratioX * gameContainer.getInput().getMouseX();
		double y = ratioY * gameContainer.getInput().getMouseY();
		return new double[]{x,y};
	}
	public void redefineColorbasedonType(Graphics g, int type){
		switch (type) {
		case ParticleForce.TYPE_GRAVITATIONAL:
			g.setColor(Color.red);
			break;
		case ParticleForce.TYPE_FASTGRAVITATIONAL:
			g.setColor(Color.orange);
			break;
		case ParticleForce.TYPE_TWIRL:
			g.setColor(Color.green);
			break;
		case ParticleForce.TYPE_COMPLEX_RING:
			g.setColor(Color.blue);
			break;
		}
	}
	int val =0;
	boolean alteringVSeconds = false;
	@Override
	public void keyPressed(int key, char c){
		double ratioX = (double)Particle2D.VIRTUALAREA_X/(double)Particle2D.DISPLAY_WIDTH;
		double ratioY = (double)Particle2D.VIRTUALAREA_Y/(double)Particle2D.DISPLAY_HEIGHT;
		double x = ratioX * gameContainer.getInput().getMouseX();
		double y = ratioY * gameContainer.getInput().getMouseY();
		switch (key){
		case Input.KEY_H:
			helpToggled=!helpToggled;
			break;
		case Input.KEY_P:
			mode = MOUSEMODE_PLACE_ION;
			break;
		case Input.KEY_F:
			mode = MOUSEMODE_PLACE_FORCE;
			break;
		case Input.KEY_L:
			this.clearPaths();
			int sqrtlines=80;
			int simtime = 10;
			for(int x1=0; x1<sqrtlines; x1++){
				for (int y1=0; y1<sqrtlines; y1++) {
					Path p1 = new Path();
					p1.performAnalysis(x1*(500.0/sqrtlines),y1*(500.0/sqrtlines), 0, 0, Particle2D.instance.getForces(), simtime);
					this.pathsToDraw.add(p1);
				}
				
			}
			break;
		case Input.KEY_G:
			
			break;
		case Input.KEY_A:
			Path p = new Path();
			p.performAnalysis(x, y, 0, 0, Particle2D.instance.getForces(), Particle2D.PATH_SIMULATION_TIME);
			this.pathsToDraw.add(p);
			break;
		case Input.KEY_C:
			this.clearPaths();
			break;
		case Input.KEY_BACKSLASH:
				this.posToDelete = new double[]{x,y};
			break;
		case Input.KEY_ENTER:
			if(mode==MOUSEMODE_PLACE_ION){
				this.particleManager.createParticle(x, y);
			}else if(mode==MOUSEMODE_PLACE_FORCE){
				Particle2D.instance.genForce(x, y, 10, 0, 0, ParticleForce.TYPE_GRAVITATIONAL);
			}else{
				// This should not happen.
			}
			break;
		case Input.KEY_V:
			if(alteringVSeconds){
				alteringVSeconds=false;
				if (val<=0){
					return;
				}
				particleManager.tickratePerVSecond = val;
				logger.log("Ticks per VSecond altered to: "+val);
				val =0;
			}
			else{
				alteringVSeconds=true;
				val=0;
			}
			break;
		}
		int value=-1;
		switch (key){
		case Input.KEY_0:
			value=0;
			break;
		case Input.KEY_1:
			value=1;
			break;
		case Input.KEY_2:
			value=2;
			break;
		case Input.KEY_3:
			value=4;
			break;
		case Input.KEY_5:
			value=5;
			break;
		case Input.KEY_6:
			value=6;
			break;
		case Input.KEY_7:
			value=7;
			break;
		case Input.KEY_8:
			value=8;
			break;
		case Input.KEY_9:
			value=9;
			break;
		
		}
		if(value>=0 && alteringVSeconds){
			val *= 10;
			val += value;
		}
		
	}
	public Color getColorBasedOnVel(double[] data, double r, double g, double b){
		double vel = (double) Math.sqrt(
				(data[ParticleManager.PARTICLEDATA_XVEL]*data[ParticleManager.PARTICLEDATA_XVEL])+
				(data[ParticleManager.PARTICLEDATA_YVEL]*data[ParticleManager.PARTICLEDATA_YVEL])
				);
		double r1 = Math.max(255, Math.min(10, r*vel));
		double g1 = Math.max(255, Math.min(10, g*vel));
		double b1 = Math.max(255, Math.min(10, b*vel));
		return new Color((float)r1,(float)g1,(float)b1);
	}
	private boolean inside(double val, double min, double max){
		return (min<val && val<max);
	}
	double[] posToDelete = {-1,-1};
	public void fullydeleteselectedforces(){
		if(posToDelete[0]>0){
			this.renderingPaused=true;
			double x = posToDelete[0];
			double y = posToDelete[1];
			int pos =0;
			for(ParticleForce f : Particle2D.instance.getForces()){
				double[] data = f.getData();
				double px = data [ParticleForce.FORCEDATA_XPOS];
				double py = data [ParticleForce.FORCEDATA_YPOS];
				if (inside(px,x-5,x+5) && inside(py,y-5,y+5)){
					Particle2D.instance.getForces().remove(pos);
					return;
				}
				pos++;
			}
			this.renderingPaused=false;
			
		}
		posToDelete[0]=-1;
	}

}
